<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('profile');
});

// Route::get('/', function () {
// 	return view ('a');
// });

// Route::get('/', function () {
// 	return view ('b');
// });

// Route::get('/', function () {
// 	return view ('c');
// });

Route::get('/welcome', 'Controller@index');